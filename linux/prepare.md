# Servidor WEB (Ubuntu + Nginx + Python/Django + Postgres + Supervisor)

Nesse artigo iremos verificar como criar um servidor web completo para servir uma aplicação web django com banco de dados postgres.

> Vamos utilizar uma distribuição debian (Ubuntu 18.04)

## Atualização e estrutura

Antes de iniciarmos qualquer instalação é recomendado que o sistema operacional estaja atualizado. Para isso, logado como `root`, utilize os seguintes comandos.

```bash
add-apt-repository universe
apt update
apt upgrade
```

### SWAP 

Para evitarmos falhas de memória (JS HEAP), vamos adicionar um swap na maquina (caso não tenha).

Primeiro, vamos pular pro root

```bash
sudo su
```

Agora, vamos verificar os parametros já existentes de memória
```bash
swapon -s
free -m
df -h
```

Agora vamos criar e configurar o swap
```bash
fallocate -l 4G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo /swapfile   none    swap    sw    0   0 >> /etc/fstab
cat /proc/sys/vm/swappiness
cat /proc/sys/vm/vfs_cache_pressure
sysctl vm.vfs_cache_pressure=50
sysctl vm.swappiness=10
echo vm.swappiness=10 >> /etc/sysctl.conf
echo vm.vfs_cache_pressure=50 >> /etc/sysctl.conf
```

Não custa nada confirmar se deu tudo certo

```bash
swapon -s
free -m
cat /proc/sys/vm/swappiness
cat /proc/sys/vm/vfs_cache_pressure
cat /etc/fstab
```

## Usuário

Por questões de segurança, vamos criar um usuário onde o mesmo terá as permissões para efetuar as tarefas e não necessite utilizar o `root`. Para isso rode o comando abaixo para criar o usuario `deploy` e logo após altere a senha do novo usuário

```bash
useradd -m -d /home/deploy -G adm,www-data,sudo -s /bin/bash deploy
passwd deploy
```

Caso precise confirmar as permissões do usuário, execute o seguinte comando `cat /etc/passwd | grep deploy` e o resultado deve ser algo parecido como:

```bash
deploy:x:1001:1001::/home/deploy:/bin/bash
```

## Git

Para gerenciar nossos projetos e versões do mesmo iremos utilizar o programa `git`. Para isso instale o mesmo como o comando `sudo apt install git -y`

Agora, vamos efetuar algumas configurações padrões de nome e email do git. Para isso utilize o seguinte comando.

```bash
sudo apt install git
git config --global user.name "William Galleti"
git config --global user.email "william.galleti@gmail.com"
```

Feito isso, caso você utilize chave ssh para gerênciar suas conexões, siga os passos abaixo.

```bash
ssh-keygen -t rsa -C "william.galleti@gmail.com"
```

## Postgres

Para instalar o postgres, rode o seguinte comando:

```bash
sudo apt update
sudo apt install postgresql postgresql-contrib libpq-dev
```

Para testar se esta tudo correto, rode o seguinte comando.

```bash
sudo -u postgres psql -c "SELECT version();"
```

Para ativar o acesso remoto do banco de dados, edite o arquivo de configuração `sudo nano /etc/postgresql/10/main/postgresql.conf`

Onde estiver o trecho

```
# listen_addresses = 'localhost'                  # what IP address(es) to listen on;
```

Altere para

```
listen_addresses = '*'                  # what IP address(es) to listen on;
```

Agora para liberar o acesso remoto, vamos editar o arquivo hba `sudo nano /etc/postgresql/10/main/pg_hba.conf`

Onde estiver o trecho

```
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
```

Altere para

```
# IPv4 local connections:
host    all             all             0.0.0.0/0            md5
```

Reinicie o serviço do postgres para aplicar as alterações `sudo /etc/init.d/postgresql restart`

Para alterar a senha do usuário padrão do postgres, execute o seguinte comando

```bash
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'postgres'"
```

Feito isso o banco está apto a receber conexões externas (exemplo PGadmin) e funcionando.

## MySQL

Agora vamos ao MySQL (Caso precise....)

```bash
sudo apt install mysql-server libmysqlclient-dev
sudo mysql_secure_installation
```

Após efetuar a configuração de segurança, vamos habilitar a conexão remota. Para isso edite o arquivo `sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf`
```
...
# Instead of skip-networking the default is now to listen only on
# localhost which is more compatible and is not less secure.
bind-address            = 0.0.0.0
...
```

Reinicie o serviço `sudo /etc/init.d/mysql restart`

Agora, vamos criar um usuário para não utilizar o root:

```bash
sudo mysql -u root
> CREATE USER 'deploy'@'%' IDENTIFIED BY '*****';
> GRANT ALL PRIVILEGES ON * . * TO 'deploy'@'%';
```


## Frontend

Para podermos efetuar os builds dos projetos Vuejs iremos precisar do node e do yarn. Para isso, vamos utilizar o seguinte comando para instalar o nodejs.


```bash
sudo apt install curl gcc g++ make -y
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt install nodejs -y
```

E para instalar o yarn

```bash
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update 
sudo apt install yarn -y
```

## Backend

Para que o backend funcione, iremos precisar do `pyenv`, `uwsgi` e `pipenv`. Para rodar esses programas, iremos necessitar das seguintes depências.

```bash
sudo apt-get install -y build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm gettext libncurses5-dev tk-dev tcl-dev blt-dev libgdbm-dev git python-dev python3-dev aria2
```

### Pyenv

Para instalar o pyenv vamos utilizar o git

```bash
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
echo '' >> ~/.bashrc
echo '# PYENV' >> ~/.bashrc
echo 'export PATH="~/.pyenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init --path)"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc
source ~/.bashrc
```

Para testar rode o comando `pyenv -v`

### Uwsgi e pipenv

Para instalar o `uwsgi` e `pipenv` necessitamos instalar o python via pyenv e logo depois, instalá-los via `pip` globalmente na maquina. Para isso iremos rodar o seguinte comando para instalar o python


```bash
pyenv install 3.7.3
pyenv global 3.7.3
python -V
```

Agora rode o comando pip

```bash
pip install uwsgi pipenv
```
Agora vamos configurar o pipenv

```bash
echo "export PIPENV_VENV_IN_PROJECT=1" >> ~/.bashrc
source ~/.bashrc
```

## Macetes

Vamos deixar alguns `alias` para facilitar nossa vida

```bash
echo "" >> ~/.bashrc
echo "# ALIAS" >> ~/.bashrc
echo "alias manage='python \$VIRTUAL_ENV/../manage.py'" >> ~/.bashrc
echo "alias pps='pipenv shell'" >> ~/.bashrc
```

## Supervisor

Para manter o software uwsgi sempre funcionando, iremos utilizar o supervisor. Para isso, rode o seguinte comando.

```bash
sudo apt install supervisor -y
```

## Nginx

Para instalar o nginx utilize o comando

```bash
sudo apt install nginx -y
```

Vamos dar uma turbinada no nginx, para evitar alguns problemas mais adiante.

Edite o arquivo `sudo nano /etc/nginx/nginx.conf`:

```conf
worker_processes 4; # Quantidade de nucleos da maquina
pid /var/run/nginx.pid; # Mudar o caminho do pid para multiplos servers
```

Agora o arquivo `sudo nano /etc/init.d/nginx`:

```
NGINX_PID=/var/run/nginx.pid
```

Reinicie o servidor com o comando `sudo /etc/init.d/nginx restart`

## Certbot

Para instalar nossos certificados https, iremos utiliar o certbot. Para isso, iremos rodar o seguinte comando.

```bash
sudo apt install -y software-properties-common
sudo add-apt-repository -y ppa:certbot/certbot
sudo apt update
sudo apt install -y python-certbot-nginx 
```

## VNC

Caso necessite rodar o vnc para acesso remoto, utilize o seguinte comando.

```bash
sudo apt install xfce4 xfce4-goodies tightvncserver
```

## Monitor

Para monitorar nosso server vamos instalar o `netdata`, `htop` e `nmon`

### Netdata

```bash
sudo apt install zlib1g-dev uuid-dev libmnl-dev gcc make git autoconf autoconf-archive autogen automake pkg-config curl -y
git clone https://github.com/firehol/netdata.git --depth=1
cd netdata
sudo ./netdata-installer.sh
```

### Nmon e htop

```bash
sudo apt install nmon htop -y
echo "export NMON='cmd'" >> ~/.bashrc
source ~/.bashrc
```
