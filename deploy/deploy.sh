#!/bin/bash

set -e

# pull
cd /home/ubuntu/projects/wTutoriais
git reset --hard HEAD
git pull origin master --force

yarn
vuepress build